# A simple project that classifies the sentiment of tweets

The project requires a config.py file with the following data:
```
CONSUMER_KEY = 'XXXXX'
CONSUMER_SECRET = 'XXXXX'
ACCESS_TOKEN = 'XXXXX'
ACCESS_SECRET = 'XXXXX'
```

These can be obtained by creating a twitter app at https://apps.twitter.com.