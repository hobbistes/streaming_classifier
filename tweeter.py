import os
import time
import datetime
import json
import tweepy
import config as cfg

from tweepy.streaming import StreamListener

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
import pickle

class TwitterHandler(object):
    """ A class that to interact with the Twitter"""

    def __init__(self):
        auth = tweepy.OAuthHandler(cfg.CONSUMER_KEY, cfg.CONSUMER_SECRET)
        auth.set_access_token(cfg.ACCESS_TOKEN, cfg.ACCESS_SECRET)

        self.api = tweepy.API(auth)
        self.auth = auth

    def get_tweets(self):
        public_tweets = self.api.home_timeline()
        for tweet in public_tweets:
            print(tweet.text)

 
class MyListener(StreamListener):
 
    def __init__(self, auth, path):
        self.auth = auth
        with open(path+'vectorizer.pkl', 'rb') as f:
            self.vectorizer = pickle.load(f)
        with open(path+'logregclf.pkl', 'rb') as f:
            self.logregclf = pickle.load(f)
        with open(path+'naiveclf.pkl', 'rb') as f:
            self.naiveclf = pickle.load(f)
        with open(path+'svmclf.pkl', 'rb') as f:
            self.svm = pickle.load(f)
        with open(path+'knnclf.pkl', 'rb') as f:
            self.knn = pickle.load(f)

    def on_data(self, data):
        try:
            print(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))
            tweet = json.loads(data)['text']
            tweet_tr = self.vectorizer.transform([tweet])
            print('Original tweet:', tweet)
            print('Prediction Logistic Regression:', self.logregclf.predict(tweet_tr))
            print('Prediction Naive Bayes:', self.naiveclf.predict(tweet_tr))
            print('Prediction SVM:', self.svm.predict(tweet_tr))
            # print('Prediction kNN:', self.knn.predict(tweet_tr))
            print()
        except BaseException as e:
            print("Error on_data: %s" % str(e))
        return True
 
    def on_error(self, status):
        print(status)
        return True
 
