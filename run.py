import config as cfg
import tweepy
from tweepy import Stream
from tweeter import TwitterHandler, MyListener

def main():
    auth = tweepy.OAuthHandler(cfg.CONSUMER_KEY, cfg.CONSUMER_SECRET)
    auth.set_access_token(cfg.ACCESS_TOKEN, cfg.ACCESS_SECRET)

    twitter_stream = Stream(auth, MyListener(auth, '../models/model_2018-02-28-23-14-33/'))
    twitter_stream.filter(track=['#bitcoin'])


if __name__ == '__main__':
    main()